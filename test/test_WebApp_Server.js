"use strict";

import { expect } from "chai";
import { afterEach, beforeEach, describe, it } from "mocha";
import axios from "axios";

import AppServer from "../src/index.js";

describe("WebApp Server", function() {
  let env = {};

  beforeEach(async function() {
    await AppServer.listen();
    env.addr = AppServer.address();
  });

  afterEach(function() {
    AppServer.close();
    env = {};
  });

  it("serves static resources", async function() {
    const res = await axios.get(`http://localhost:${env.addr.port}/dist/img/favicon.svg`);
    expect(res.status).to.equal(200);
    expect(res.data).to.include("</svg>");
  });
});
