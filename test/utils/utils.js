import { isNil, makeDeferred, timeout } from "@cern/nodash";
import { expect } from "chai";
import d from "debug";

const debug = d("test:utils");

/**
 * @param  {() => boolean|any} cb
 * @param {string} [msg]
 * @param  {number} [timeout=1000]
 * @return {Promise<any>}
 */
export function waitFor(cb, msg = undefined, timeout = 1000) {
  var def = makeDeferred();
  var resolved = false;
  var err = new Error(msg || "timeout"); /* create this early to have stacktrace */
  /** @type {NodeJS.Timeout|null} */
  var nextTimer = null;
  var timer = setTimeout(() => {
    resolved = true;
    // @ts-ignore
    clearTimeout(nextTimer);
    def.reject(err);
  }, timeout ?? 1000);


  function next() {
    if (resolved) { return; }
    try {
      var ret = cb(); /* eslint-disable-line callback-return */
      if (ret) {
        clearTimeout(timer);
        resolved = true;
        def.resolve(ret);
      }
      else {
        nextTimer = /** @type {any} */(setTimeout(next, 200));
      }
    }
    catch (e) {
      clearTimeout(timer);
      resolved = true;
      def.reject(!isNil(e?.message) ? e : new Error(e));
    }
  }

  next();
  return def.promise;
}

/**
 * @param  {Function} fun
 */
function throws(fun) {
  try {
    fun();
    return null;
  }
  catch (e) {
    return e.message;
  }
}

/**
 * @template T=any
 * @param  {() => T} test
 * @param  {any} val
 * @param  {string} [msg]
 * @param  {number} [timeout]
 * @return {Promise<T>}
 */
export function waitForValue(test, val, msg = undefined, timeout = undefined) {
  /** @type {any} */
  var _val;
  return waitFor(() => {
    _val = test();
    return throws(() => expect(_val).to.deep.equal(val)) === null;
  }, msg, timeout)
  .catch((err) => {
    const assertMsg = throws(() => expect(_val).to.deep.equal(val)) ||
      ("Invalid result value: " + (_val?.toString?.() ?? _val) + " != " + val);
    if (err instanceof Error) {
      err.message = msg ? `${assertMsg}: (${msg})` : assertMsg;
      throw err;
    }
    throw new Error(msg);
  });
}

/**
 * @param {any} obj
 * @param {string} evt
 * @param {number} [count]
 * @param {number} [ms]
 * @returns {Promise<any>}
 */
export function waitForEvent(obj, evt, count, ms) {
  var def = makeDeferred();
  const stack = (new Error()).stack;
  var ret = [];
  var received = 0;
  count = count ?? 1;

  var cb = function(e) {
    ret.push(e);
    debug(`waitForEvent: received:${evt}`);

    if (++received >= count) {
      def.resolve((count === 1) ? ret[0] : ret);
    }
  };
  obj.on(evt, cb);

  return timeout(def.promise, ms ?? 1000)
  .catch((err) => {
    err.stack = stack;
    throw err;
  })
  .finally(function() {
    if (def.isPending) { def.reject(); }
    obj.removeListener(evt, cb);
  });
}

