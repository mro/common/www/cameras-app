// @ts-check

import Vue from "vue";
import { mapGetters, mapState } from "vuex";

import CameraControl from "./CameraControl.vue";

export default Vue.extend({
  name: "CameraGroup",
  components: { CameraControl },
  computed: {
    /**
     * @type {{
     *    page(): string|null,
     *    loading(): boolean
     * }}
     */
    ...mapState([ "page", "loading" ]),
    /** @type {{ camConfig(): object }} */
    ...mapState("cameras", { camConfig: "cameras" }),
    /**
     * @returns {string[]}
     */
    cameras() { return this.camConfig?.[this.page]?.cameras ?? []; },
    .../** @type {isOperator(): boolean, isExpert(): boolean} */mapGetters([
      "isOperator", "isExpert"
    ]),
    /**
     * @return {boolean}
     */
    canControl() {
      return this.isOperator || this.isExpert;
    }
  }
});
