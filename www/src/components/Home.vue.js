// @ts-check

import Vue from "vue";
import { mapState } from "vuex";

/**
 * @typedef {V.Instance<typeof component> & { $butils: BaseVue.butils }} Instance
 */

const component = Vue.extend({
  name: "Home",
  computed: {
    /** @type {{ loading(): boolean }} */
    ...mapState([ "loading" ]),
    /** @type {{ camConfig(): object }} */
    ...mapState("cameras", { camConfig: "cameras" })
  }
});
export default component;
