// @ts-check

import Vue from "vue";
import CameraCard from "./CameraCard.vue";
import Card from "../Card.vue";
import { debounce, forEach, throttle } from "lodash";
import { mapGetters } from "vuex";
import { sources } from "../store";
import { DateTime } from "luxon";
import d from "debug";

const debug = d("cam");

/**
 * @typedef {import("@cern/base-vue").BaseToggle} BaseToggle
 * @typedef {{
 *  cam: V.Instance<typeof CameraCard>,
 *  card: V.Instance<typeof Card>,
 *  focus: HTMLInputElement,
 *  iris: HTMLInputElement,
 *  autofocus: V.Instance<BaseToggle>,
 *  autoiris: V.Instance<BaseToggle>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>>} Instance
 */
const component = /** @type {V.Constructor<any, Refs>} */(Vue).extend({
  name: "CameraControl",
  components: { CameraCard, Card },
  props: {
    group: { type: String, default: "" },
    camera: { type: String, default: "" },
    description: { type: String, default: "" },
    ptz: { type: Boolean, default: false }
  },
  /**
   * @return {{
   *   selected: null|CamExpress.CameraInfo, refreshing: boolean, advanced: boolean,
   *   position: any , limits: any, paused: boolean, screenshot: string,
   *   screenshotName: string, showControls: boolean, isFocused: boolean
   *  }}
   */
  data() {
    return { selected: null, refreshing: false, advanced: false, paused: false,
      position: null, limits: null, screenshot: "", screenshotName: "",
      showControls: false, isFocused: false
    };
  },
  computed: {
    .../** @type {{isOperator(): boolean, isExpert(): boolean}} */(mapGetters([
      "isOperator", "isExpert"
    ])),
    /**
     * @this {Instance}
     * @return {boolean}
     */
    canControl() { return this.isOperator || this.isExpert; }
  },
  /** @this {Instance} */
  mounted() {
    this.wheelMove = debounce(this.move, 300);
    this.move = throttle(this.move, 250);
    this.refreshAdvanced();

    this.showControls = this.$refs?.cam?.connected ?? false;
  },
  methods: {
    /** @param {any} params */
    async move(params, absolute = true) {
      if (!this.canControl) { return; }
      if (this.paused) { return; }
      debug("ptz move request", params);
      await sources.cam.movePTZ(this.group, this.camera, params, absolute);
    },
    /** @param {MouseEvent} event */
    async moveToPointer(event) {
      if (!this.canControl) { return; }
      if (this.paused ||
          !this.$refs.cam?.connected ||
          !this.isFocused || !this.$refs?.card?.isFocused ||
          !this.ptz) {
        return;
      }

      event?.preventDefault();
      event?.stopImmediatePropagation();
      const rect = /** @type {Element} */ (event.target)?.getBoundingClientRect?.();
      await sources.cam.movePTZ(this.group, this.camera, {
        imagewidth: Math.trunc(rect.width),
        imageheight: Math.trunc(rect.height),
        center: `${Math.trunc(event.clientX - rect.x)},${Math.trunc(event.clientY - rect.y)}`
      })
      .then(() => {
        if (this.advanced) {
          this.refreshAdvanced();
        }
      });
    },
    onMouseDown() {
      // 'click' event occurs always after 'focusin' one: let's check if the
      // card is already focused before a complete mouse click (down + up)
      this.isFocused = this.$refs?.card?.isFocused ?? false;
    },
    /**
     * @this {Instance}
     * @param {WheelEvent} event
     */
    onWheel(event) { /* eslint-disable-line complexity */
      if (!this.canControl) { return; }
      if (this.paused || !this.$refs.cam?.connected ||
        !this.$refs?.card?.isFocused || !this.ptz) { return; }

      event?.preventDefault();
      event?.stopImmediatePropagation();

      /** @type {number} */let zoom = (this.position?.zoom ?? 0);
      /** @type {number} */const zoomMin = (this.limits?.MinZoom ?? 0);
      /** @type {number} */const zoomMax = (this.limits?.MaxZoom ?? 9999);
      /** @type {number} */let delta = Math.abs(zoomMin - zoomMax);
      delta /= (event.ctrlKey ? 250 : 40);

      zoom += (event.deltaY > 0) ? -delta : delta;
      zoom = Math.max(zoomMin, Math.min(zoomMax, zoom));

      Object.assign(this.position ?? {}, { zoom });
      this.wheelMove?.({ zoom }, true);
    },
    /**
     * @this {Instance}
     * @param {any} position
     */
    updateAdvanced(position) {
      let input = this.$refs.autofocus;
      if (input) { input.editValue = position.autofocus; }

      input = this.$refs.autoiris;
      if (input) { input.editValue = position.autoiris; }

      forEach([ "pan", "tilt", "zoom", "brightness", "focus", "iris" ], (p) => {
        const input = this.$refs[p];
        if (input) { input.value = position[p]; }
      });
      this.$refs.focus?.setAttribute?.("disabled", position["autofocus"] ? "disabled" : "");
      this.$refs.iris?.setAttribute?.("disabled", position["autoiris"] ? "disabled" : "");
    },
    async refreshAdvanced() {
      if (this.refreshing || !this.ptz) { return; }
      this.refreshing = true;
      try {
        this.position = await sources.cam.getPTZ(this.group, this.camera);
        this.limits = await sources.cam.getPTZLimits(this.group, this.camera);
        this.updateAdvanced(this.position);
      }
      finally {
        this.refreshing = false;
      }
    },
    /** @this {Instance} */
    async toggleAdvanced() {
      this.advanced = !this.advanced;
      if (this.advanced) {
        this.refreshAdvanced();
      }
    },
    /**
     * @this {Instance}
     * @param {string} name
     */
    async toggleCtrl(name) {
      if (this.paused) { return; }
      await this.move({ [name]: !this.$refs[name]?.editValue });
      this.refreshAdvanced();
    },
    /** @this {Instance} */
    async togglePause() {
      const tmp = this.screenshot;
      this.paused = !this.paused;

      if (this.paused) {
        await sources.cam.getScreenshot(this.group, this.camera)
        .then(
          (ret) => {
            this.screenshot = URL.createObjectURL(ret);
          },
          () => {
            if (this.$refs.cam?.imgURL) {
              this.screenshot = this.$refs.cam.imgURL;
              this.$refs.cam.imgURL = "";
            }
          })
        .then(() => {
          if (!this.screenshot) { throw new Error("no image"); }

          const timestamp = DateTime.now()
          .set({ millisecond: 0 })
          .toISO({
            format: "extended",
            suppressMilliseconds: true,
            includeOffset: false
          });
          this.screenshotName = `${this.group}_${this.camera}_${timestamp}.jpeg`;
        })
        .catch((err) => console.warn("saving screenshot failed: ", err));
      }
      else {
        this.screenshot = "";
        this.screenshotName = "";
      }
      URL.revokeObjectURL(tmp);
    }
  }
});

export default component;
