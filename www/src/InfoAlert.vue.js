// @ts-check
import Vue from "vue";

export default Vue.extend({ name: "InfoAlert" });
