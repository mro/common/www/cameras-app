
export = AppStore
export as namespace AppStore

declare namespace AppStore {
  interface State {
    user: { username: string, [key: string]: any } | null,
    loading: boolean,
    page: string | null
  }

  interface UiState {
    showKeyHints: boolean
  }

  interface CameraConfig {
    name: string
    ptz?: boolean
    description?: string
  }

  interface CamerasData {
    cameras: {
      [page: string]: {
        description?: string,
        cameras: CameraConfig[]
      }
    }
  }
}
