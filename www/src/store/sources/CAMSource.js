// @ts-check
import axios from "axios";
import { get } from "lodash";

import { BaseLogger, butils } from "@cern/base-vue";
import d from "debug";
import { makeUri } from "../../utilities";

const debug = d("app:sources");

export default class CAMSource {
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store) {
    /** @type {V.Store<AppStore.State>} */
    this.store = store;
  }

  async connect() {
    /** @type {string[]} */
    const camConfig = await axios.get(
      makeUri(butils.currentUrl(), "/config").href())
    .then(
      (ret) => ret.data,
      /** @param {Error} err */
      (err) => BaseLogger.error(`failed to retrieve cameras list: ${err}`));

    this.store.commit("cameras/update", { cameras: camConfig });
  }

  /**
   * @param {string} group
   * @param {string|AppStore.CameraConfig} camera
   */
  async getPTZLimits(group, camera) {
    /** @type {string} */
    const name = get(camera, [ "name" ], camera);
    const limits = await axios.get(makeUri(
      butils.currentUrl(),
      `/cameras/${group}/${name}/ptz/limits`).href())
    .then(
      (ret) => ret.data,
      (err) => BaseLogger.error(`failed to retrieve cameras limits: ${err}`));

    return limits;
  }

  /**
   * @param {string} group
   * @param {string|AppStore.CameraConfig} camera
   */
  async getPTZ(group, camera) {
    /** @type {string} */
    const name = get(camera, [ "name" ], camera);
    return await axios.get(makeUri(butils.currentUrl(),
      `/cameras/${group}/${name}/ptz`).href())
    .then(
      (ret) => ret.data,
      (err) => BaseLogger.error(`failed to retrieve cameras limits: ${err}`));
  }

  /**
   * @param {string} group
   * @param {string} camera
   * @param {any} params
   * @param {boolean} absolute
   */
  async movePTZ(group, camera, params, absolute = true) {
    debug("moving PTZ", params);
    /** @type {string} */
    if (absolute) {
      await axios.put(makeUri(
        butils.currentUrl(),
        `/cameras/${group}/${camera}/ptz`).href(), params)
      .catch((err) => BaseLogger.error(`failed to move camera: ${err}`));
    }
    else {
      await axios.post(makeUri(
        butils.currentUrl(),
        `/cameras/${group}/${camera}/ptz`).href(), params)
      .catch((err) => BaseLogger.error(`failed to move camera: ${err}`));
    }
  }

  /**
   * @param {string} group
   * @param {string} camera
   * @return {Promise<Blob>}
   */
  async getScreenshot(group, camera) {
    return axios.get(
      makeUri(
        butils.currentUrl(),
        `/cameras/${group}/${camera}/screenshot`).href(),
      { responseType: "blob" }).then((ret) => ret.data);
  }
}
