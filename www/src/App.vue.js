// @ts-check

import { TITLE, VERSION } from "./Consts";
import Vue from "vue";
import { forEach, get, isArray, isEmpty, isNil, pullAllBy } from "lodash";
import { mapState } from "vuex";

import { install } from "./utilities";
import contributors from "../../Contributors.yml";
import libs from "../../Components.yml";

const options = { contributors, libs };


install(Vue);
/**
 * @typedef {V.Instance<typeof component>} Instance
 */
const component = Vue.extend({
  name: "App",
  ...options,
  data() { return { TITLE, VERSION, showRoutes: false }; },
  computed: {
    .../** @type { page(): string|null } */mapState([ "page" ]),
    .../** @type {{ camConfig(): object }} */mapState(
      "cameras", { camConfig: "cameras" })
  },
  /** @this {Instance} */
  mounted() {
    // sets dark theme
    /** @type {string} */
    const theme = get(this.$route.query, "theme");
    if (!isNil(theme)) {
      this.$store.commit("ui/update", { darkMode: theme === "dark" });
      this.$store.dispatch("ui/applyTheme");
    }

    // retrieve camera configuration
    this.$store.commit("update", { loading: true });
    this.$store.sources.cam.connect()
    .then(() => {
      // update routes
      if (this.$router && !isEmpty(this.camConfig)) {
        /** @type {{ name: string, path: string }[]} */
        const pages = [];
        forEach(this.camConfig, (cams, page) => {
          if (!isEmpty(cams)) {
            pages.push({ name: page, path: `/${page}` });
          }
        });

        if (!isEmpty(pages) &&
          isArray(get(this.$router, [ "options", "routes" ]))) {
          // remove any duplicate
          pullAllBy(this.$router.options.routes, pages, "name");

          this.$router.options.routes.push(...pages);

          // navigate to the first page
          if (this.$router.currentRoute?.path === "/") {
            this.$router.push(pages[0]?.path || "/");
          }

          this.showRoutes = true;
        }
      }
    })
    .finally(() => {
      this.$store.commit("update", { loading: false });
    });
  }
});

export default component;
