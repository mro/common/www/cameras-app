// @ts-check

import URI from "urijs";

let id = 0;
/**
* @return {string}
*/
export function genId() {
  return "a-" + (++id);
}

/**
 * @param {string} url
 */
export function httpToWs(url) {
  return url.replace(/^http(s)?\:\/\//, "ws$1://");
}

/**
 * @param  {string[]} args
 * @returns {URI}
 */
export function makeUri(...args) {
  return new URI(args.join("/")).normalize();
}

/**
 * @param {Vue.VueConstructor<any>} Vue
 */
export function install(Vue) {
  // @ts-ignore
  Vue.prototype.$makeUri = makeUri;
}

/**
 *
 * @param {Vue} object
 * @param {Function} before
 * @param {Function} after
 * @param {(name: string) => boolean} [filter]
 */
export function methodWrapper(object, before, after, filter = undefined) {
  Object.entries(object.$options.methods ?? {}).forEach(([ name, func ]) => {
    if (filter && !filter(name)) {
      return;
    }

    // debug("wrapping %s", name);
    /* @ts-ignore */
    object[name] = async function(...args) {
      await before?.(name);
      try {
        const ret = await func.apply(this, args);
        await after?.(name);
        return ret;
      }
      catch (err) {
        await after?.(name);
        throw err;
      }
    };
  });
}

export default { install };
