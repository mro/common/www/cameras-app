// @ts-check

const
  { set } = require("@cern/nodash"),
  url = require("url"),
  { readFileSync } = require("fs"),
  path = require("path");

/**
 * @param  {any} env
 */
async function createApp(env) {
  const MJPEGServer = (await import("../../test/utils/MJPEGServer.js")).default;
  const Server = (await import("../../src/Server.js")).default;
  const { camApp, stubs } = (await import("../../src/config-stub.js")).default;

  env.sampleJPEG =
    readFileSync(path.join(__dirname, "..", "dist", "img", "favicon.jpg"));

  // starting the MJPEG-Server Stub
  env.mjpegServer = new MJPEGServer(stubs);
  await env.mjpegServer.listen();

  // update camera config with target server port
  /** @type {number} */
  const port = env.mjpegServer.address().port;
  const groups = Object.entries(camApp.groups)
  .reduce((ret, [ page, group ]) => {
    set(ret, [ page, "cameras" ], []);

    Object.values(group.cameras ?? []).forEach((cam) => {
      ret[page].description = group?.description;
      ret[page].cameras.push({
        name: cam.name,
        url: `http://localhost:${port}` + url.parse(cam.url, true).path || ""
      });
    });
    return ret;
  }, {});

  env.server = new Server({
    title: "", port: 0, basePath: "",
    camApp: { groups, credentials: camApp.credentials }
  });

  return env.server.listen();
}

module.exports = { createApp };
