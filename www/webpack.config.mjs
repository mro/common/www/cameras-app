// @ts-check

import path from "node:path";
import webpack from "webpack";
import yaml from "js-yaml";
import { set } from "@cern/nodash";
import { execSync } from "node:child_process";

import { VueLoaderPlugin } from "vue-loader";
import CompressionPlugin from "compression-webpack-plugin";
import { GenCreditsPlugin } from "@cern/ci-utils";
import { fileURLToPath } from "node:url";

const dirname = path.dirname(fileURLToPath(import.meta.url));

const config = {
  mode: 'development',
  entry: path.resolve('./src/index.js'),
  output: {
    path: path.resolve(dirname, './dist'),
    publicPath: '/dist/',
    filename: 'index.js',
    hashFunction: 'xxhash64'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      },
      {
        test: /\.scss$/,
        use: [ 'style-loader', 'css-loader', 'sass-loader' ]
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.pug$/,
        loader: 'pug-plain-loader'
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        options: {
          extends: path.join(dirname, '.babelrc')
        }
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        type: "asset/resource",
        generator: {
          filename: "fonts/[name].[ext]"
        }
      },
      {
        test: /\.ya?ml$/,
        type: 'json',
        parser: { parse: yaml.load }
      },
      {
        test: /\.shared-worker\.js/,
        loader: 'worker-loader',
        options: {
          filename: '[name].js',
          worker: {
            type: 'SharedWorker',
            options: { name: 'worker' }
          }
        }
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      'bootstrap': 'bootstrap/dist/js/bootstrap.bundle.min.js'
    },
    extensions: [ '.js', '.vue', '.json' ]
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: "warning"
  },
  devtool: 'inline-cheap-module-source-map',
  plugins: [
    new webpack.ProvidePlugin({
      Buffer: [ 'buffer', 'Buffer' ]
    }),
    new VueLoaderPlugin(),
    new webpack.DefinePlugin({
      VERSION: JSON.stringify(execSync("git describe --tags --always HEAD").toString().trim()),
      TITLE: JSON.stringify((await import("../src/config.js")).default.title)
    }),
    {
      apply: (compiler) => {
        compiler.hooks.beforeCompile.tap('IstanbulPatch', () => {
          /* see https://github.com/istanbuljs/nyc/issues/718 for details */
          execSync("sed -i='tmp' 's/source: pathutils.relativeTo(start.source, origFile),/source: origFile,/' node_modules/istanbul-lib-source-maps/lib/get-mapping.js")
        });
      }
    },
    new GenCreditsPlugin(
      [ 'express' ],
      [ '@cern/base-vue', 'vue', 'vuex', 'd3', 'bootstrap',
        '@fortawesome/fontawesome-free' ])
  ]
}

if (process.env.NODE_ENV === "production") {
  config.devtool = "source-map";
  config.mode = "production";
  set(config, [ "optimization", "nodeEnv" ], "production");
  set(config, [ "optimization", "moduleIds" ], "named");

  config.plugins?.push(
    new CompressionPlugin({ algorithm: "gzip", threshold: 10240, minRatio: 0.8 }),
    new CompressionPlugin({ algorithm: "brotliCompress", filename: "[path][base].br", threshold: 10240, minRatio: 0.8 }));
}

export default config;
