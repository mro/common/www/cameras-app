// @ts-check

import d from "debug";
import passport from "passport";
import cookieSession from "cookie-session";
import { Issuer, Strategy } from "openid-client";

const debug = d("app:auth");

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').NextFunction} NextFunction
 * @typedef {import('express').IRouter} IRouter
 */

/* to serialize info in session */
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

/**
 * @param {string|string[]} role required role or list of roles (one of)
 * @param {Request} req incoming request
 * @param {Response} res outgoing reply
 * @param {NextFunction} next routing next callback
 */
// eslint-disable-next-line complexity
export async function roleCheck(role, req, res, next) {
  const isStringRole = typeof role === "string";
  const userRoles = req?.user?.cern_roles ?? [];

  if (!req.user) {
    debug("unauth user -> /auth", req.path);
    return res.redirect(`${req.baseUrl ?? ""}/auth`);
  }
  else if (isStringRole) {
    if (userRoles.includes(role)) {
      return next();
    }
  }
  else if ((role ?? []).some((r) => userRoles.includes(r))) {
    return next();
  }

  debug("permission denied");
  res.status(403);
  return res.render("denied", { baseUrl: req.baseUrl, role });
}

/**
 *
 * @param {IRouter} app
 * @param {AppServer.Config} config
 * @param {(user: any) => any} [loginCallback]
 */
export async function register(app, config, loginCallback) {
  var basePath = config?.basePath ?? "";

  const oidcIssuer =
    await Issuer.discover("https://auth.cern.ch/auth/realms/cern");
  const client = new oidcIssuer.Client({
    client_id: config?.auth?.clientID ?? "", /* eslint-disable-line camelcase */
    client_secret: config?.auth?.clientSecret ?? "", /* eslint-disable-line camelcase */
    redirect_uris: [ config?.auth?.callbackURL ?? "" ], /* eslint-disable-line camelcase */
    post_logout_redirect_uris: [ config?.auth?.logoutURL ?? "" ] /* eslint-disable-line camelcase */
  });

  passport.use("oidc", new Strategy({ client },
    /** @type {import('openid-client').StrategyVerifyCallbackUserInfo<any>} */
    function(tokenSet, userInfo, done) {
      debug("user authenticated: %s", userInfo?.email);
      const user = tokenSet.claims();

      done(null, user);
      loginCallback?.(user);
    }));

  app.use(cookieSession({
    name: "oidc:auth.cern.ch",
    secret: config?.auth?.clientSecret ?? "default",
    path: config?.basePath || "/",
    signed: true,
    overwrite: true
  }));
  // register regenerate & save after the cookieSession middleware initialization
  // see https://github.com/jaredhanson/passport/issues/904
  app.use(function(request, _response, next) {
    if (request.session && !request.session.regenerate) {
      request.session.regenerate = (/** @type {() => any} */cb) => cb();
    }
    if (request.session && !request.session.save) {
      request.session.save = (/** @type {() => any} */cb) => cb();
    }
    next();
  });

  app.use(passport.initialize());
  app.use(passport.session());
  app.get("/auth", passport.authenticate("oidc"));
  app.get("/auth/callback", passport.authenticate("oidc", {
    successRedirect: basePath + "/",
    failureRedirect: basePath + "/auth"
  }));
  app.get("/auth/logout", function(req, res) {
    req.session = null;
    res.redirect(client.endSessionUrl());
  });
  app.get("/auth/me", function(req, res) { res.send(req.user); });
}
