// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)
// SPDX-Created: 2024-01-26T18:32:07
// SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

import { merge } from "@cern/nodash";
import yaml from "js-yaml";
import fs from "node:fs/promises";
import path from "node:path";
import d from "debug";

const debug = d("app:config");

/**
 *
 * @param {string} filename
 * @returns {Promise<string|null>}
 */
async function resolveFile(filename) {
  try {
    await fs.access(filename, fs.constants.R_OK);
    return filename;
  }
  catch {
    /* noop */
  }

  for (const ext of [ ".js", ".json", ".yaml", ".yml" ]) {
    try {
      const out = filename + ext;
      await fs.access(out, fs.constants.R_OK);
      return out;
    }
    catch {
      continue;
    }
  }
  return null;
}

/**
 * @param {string} filename
 */
export async function loadFile(filename) {
  const file = await resolveFile(path.resolve(filename));
  if (!file) {
    throw new Error(`No such file: ${filename}`);
  }
  else if (file.endsWith(".js")) {
    return (await import(file))?.default;
  }
  else {
    return yaml.load(await fs.readFile(file, "utf8"));
  }
}


/**
 * @param  {string} dir
 * @param  {RegExp} [pattern=/^config.*.yml$/] [description]
 * @return {Promise<AppServer.Config|null>}
 */
export async function loadAll(dir, pattern = /^config.*\.yml$/) {
  try {
    /** @type {string[]} */
    const files = [];
    (await fs.readdir(dir)).forEach((f) => {
      if ((f.endsWith(".json") || f.endsWith(".yml")) &&
          (!pattern || pattern.test(f))) {
        files.push(f);
      }
    });
    debug('Loading files: %o from "%s"', files, dir);
    let ret = /** @type{AppServer.Config|null} */ (null);
    for (const f of files) {
      ret = merge(ret,
        yaml.load((await fs.readFile(path.join(dir, f)))?.toString()));
    }
    return ret;
  }
  catch (e) {
    debug(/** @type {Error} */(e).message);
    return null;
  }
}

/** @type {AppServer.Config} */
const config = await loadAll("/etc/app") ?? await loadAll("./") ??
  (await import("./config-stub.js")).default;

config.port = config.port ?? 8080;
config.basePath = config.basePath ?? "";

export default /** @type {AppServer.Config} */ (config);
