
export = AppServer
export as namespace AppServer

declare namespace AppServer {
    interface Auth {
        clientID: string,
        clientSecret: string,
        callbackURL: string,
        logoutURL: string
    }
    interface Config {
        title: string,
        license?: { name: string, url?: string },
        contact?: { name: string, url?: string, email?: string },
        port?: number,
        basePath: string,
        noWebSocket?: boolean,
        auth?: Auth,
        camApp: {
            unsubscribeTimeout?: number, // seconds
            groups: { [page: string]: {
                description?: string,
                cameras: Array<CamApp.Camera>
            } },
            credentials: CamApp.Credentials
        },
        stubs?: {
            [cam: string]: {
                path: string,
                auth?: { user: string, password: string }
            }
        }
    }

    namespace CamApp {
        interface Camera {
            name: string,
            description?: string,
            url: string,
            contentLength?: boolean,
            boundary?: string,
            throttle?: number
        }
        interface Credentials {
            [key: string]: {
                user: string,
                password: string
            }
        }
    }
}

declare global {
    namespace Express {
        interface User {
            cern_roles?: string[]
        }
    }
}
