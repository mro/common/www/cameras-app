# Cameras App

A WebSite to show video streams from cameras.

This is developped using [Base Website Template](https://gitlab.cern.ch/mro/common/www/base-website-template).

# Configuration

MJPEG streams to display are defined in _deploy/config.yml_.

Below is the configuration info:

```ts
interface camApp {
  // idle timeout before unsubscribing from the camera MJPEG stream in seconds
  // (default: imminent unsubscription)
  unsubscribeTimeout?: number,
  groups: {
    [ page: string ]: {
      description?: string,
      cameras: Array<{
        // camera name
        name: string,
        // camera description
        description?: string,
        // MJPEG stream URL of camera to play
        url?: string,
        // enable the use of 'Content-Length' header field when extracting frames
        // from MJPEG stream (default: enabled)
        contentLength?: boolean
        // boundary delimiter used in MJPEG stream fragmentation
        boundary?: string, // (*)
        // string representing the separator between the header and the body of a frame (default: '\r\n\r\n')
        headerDelimiter?: string,
        // force frame detection using the JPEG magic numbers (default: false)
        jpegMagicBytes?: boolean,
        // frames throttle in milliseconds to reduce camera fps (default: not throttling)
        trottle?: number,
        // use http polling instead of streaming for input JPEGs
        poll?: boolean,
        // sets up the camera as a multiplex and rotate through the given cameras.
        multiplex?: Array<{
          // camera group
          group: string,
          // camera name
          camera: string,
          // camera display duration in milliseconds (default: 1000)
          duration?: number
        }>
      }>
    }
  },
  credentials: {
    [ camName: string ] : { // name of the relevant camera
      user: string,
      password: string
    } }
  }
}
```
Note that the configuration can be split over several files. For instance, credentials can be stored in _deploy/config-pass.yml_.

(*) Some camera stream may not fulfil the specifications properly. For details see https://datatracker.ietf.org/doc/html/rfc2046

# Documentation

# Build

To build this application application (assuming that Node.js is installed on your machine):
```bash
# Run webpack and bundle things in /dist
npm run build

# Serve pages on port 8080
npm run serve
```

# Deploy

To deploy an application, assuming that oc is configured and connected
on the proper project:
```bash
# Make sure the auth password referred to is correct
vim deploy/<app>/config-pass.yml

# Modify parameters in 'config-beta.env' file properly
oc process --local --parameters -f deploy/cameras.template.yaml # for details about parameters
vim deploy/<app>/config-beta.env

# Install 'js-yaml' with pass extension
npm install --save-dev git+https://gitlab.cern.ch/mro/common/www/js-yaml.git

# Generate the configuration and apply it:
# 1. process the deployment template file ('cameras.yaml') into a list of openshift resources;
# 2. inject the application config (including any credentials);
# 3. apply the resulting configuration to a pod
oc process --local --param-file=deploy/<app>/config-beta.env -o 'yaml' -f deploy/cameras.template.yaml > deploy/<app>/cameras-beta.yaml
oc-gen deploy/<app>/cameras-beta.yaml deploy/<app>/config*.yml | oc apply -f -
```
